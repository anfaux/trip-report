import STATUS from "../modules/constants/httpCodes.js";
export const read = async (knex, cb) => {
  let instances = await knex
    .select(["ts", "instance", "data"])
    .from("instance_list")
    .orderBy("ts", "desc");
  instances = instances.map((item) => {
    return { ...item, data: JSON.parse(item.data) };
  });
  cb(STATUS.OK, JSON.stringify(instances));
};
export default {
  read
};
