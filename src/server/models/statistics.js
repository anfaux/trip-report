import STATUS from "../modules/constants/httpCodes.js";
export const statistics = {
  read: async (knex, cb) => {
    const stats = await knex.select("*").from("statistics");
    cb(STATUS.OK, JSON.stringify(stats[0]));
  }
};
export default statistics;
