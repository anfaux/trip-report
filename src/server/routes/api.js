import * as express from "express";
import statistics from "../models/statistics.js";
import instances from "../models/instances.js";
import instance from "../models/instance.js";
const router = express.Router();
// POI
router.get("/statistics", (req, res) => {
  statistics.read(req.knex, (code, response) => {
    res.status(code).send(response);
  });
});
router.get("/instance/list", (req, res) => {
  instances.read(req.knex, (code, response) => {
    res.status(code).send(response);
  });
});
router.get("/instance/:id", (req, res) => {
  const { id } = req.params;
  instance.read(req.knex, { id }, (code, response) => {
    res.status(code).send(response);
  });
});
export default router;
