import * as express from "express";
import { envBool } from "../../modules/util.js";

const router = express.Router();
router.log = (msg) => {
  if (envBool(process.env.DEBUG)) console.log(msg);
};
router.all("*", (req, res, next) => {
  if (envBool(process.env.DEBUG) && req.params[0] !== "/up") {
    console.log(`REQ: ${req.params[0]}`);
  }
  next();
});
export default router;
