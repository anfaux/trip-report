import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

// API //////////////////
if (!process.env.REACT_APP_TRIPREPORT_SERVICE) {
  console.error("process.env.REACT_APP_TRIPREPORT_SERVICE is not undefined");
}
export const tripReportApi = createApi({
  reducerPath: "tripReportApi",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_TRIPREPORT_SERVICE}/api`,
    prepareHeaders: (headers, { getState }) => {
      headers.set("Content-type", "application/json; charset=UTF-8");
      return headers;
    },
    refetchOnMountOrArgChange: 120
  }),
  tagTypes: ["Instances"],
  endpoints: (builder) => ({
    getInstances: builder.query({
      query: () => {
        return {
          method: "GET",
          url: "instance/list"
        };
      },
      providesTags: ["Instances"]
    }),
    getInstance: builder.query({
      query: (id) => ({
        method: "GET",
        url: `instance/${id}`
      })
    }),
    getStatistics: builder.query({
      query: () => ({
        method: "GET",
        url: "statistics"
      }),
      providesTags: ["Statistics"]
    })
  })
});

// Additional Helpers //
export const up = async () => {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_TRIPREPORT_SERVICE}/api/up`,
      {
        method: "GET",
        headers: {
          Accept:
            "application/json, application/xml, text/plain, text/html, *.*",
          "Content-Type": "application/json; charset=utf-8"
        }
      }
    );
    return response.ok;
  } catch (e) {
    console.error(e);
    return false;
  }
};

export const {
  useGetInstancesQuery,
  useGetStatisticsQuery,
  useGetInstanceQuery
} = tripReportApi;
