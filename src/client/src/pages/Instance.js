import React from "react";
import { useGetInstanceQuery } from "../services/tripReportApi";
import { useParams } from "react-router-dom";
import { DateTime } from "luxon";
import PlayerList from "../components/PlayerList";
import Gallery from "../components/Gallery";
import MediaList from "../components/MediaList";
import styles from "./styles";
const Instance = () => {
  const params = useParams();
  const instanceId = params.id;
  const { data, isLoading } = useGetInstanceQuery(instanceId);

  if (!data) return null;
  if (isLoading) return <div>Loading...</div>;

  const info = data?.filter((datum) => datum.tag === "world_enter")[0].data;
  const players = data?.filter((datum) => datum.tag === "player");
  const media = data?.filter((datum) => datum.tag === "media");
  const screenshots = data?.filter((datum) => datum.tag === "screenshot");

  return (
    <div>
      <div>
        <h2>{info.name}</h2>
      </div>
      <div>
        {DateTime.fromMillis(info.tsEnter).toLocaleString(DateTime.DATE_FULL)}
      </div>
      <div>{info.tsString}</div>
      <div>{info.tsDurationString}</div>
      <div style={{ marginTop: "1rem" }}>
        {players.length > 0 && (
          <div style={styles.section}>
            <PlayerList players={players} />
          </div>
        )}
        {media.length > 0 && (
          <div style={styles.section}>
            <MediaList media={media} />
          </div>
        )}
        {screenshots.length > 0 && (
          <div style={styles.section}>
            <Gallery screenshots={screenshots} />
          </div>
        )}
      </div>
    </div>
  );
};
export default Instance;
