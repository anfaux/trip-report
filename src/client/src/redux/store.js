import { configureStore, createListenerMiddleware } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { tripReportApi } from "../services/tripReportApi";
import statusReducer from "../redux/slice/status";

const listenerMiddleware = createListenerMiddleware();

export const store = configureStore({
  reducer: {
    status: statusReducer,
    [tripReportApi.reducerPath]: tripReportApi.reducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false
    })
      .concat(tripReportApi.middleware)
      .prepend(listenerMiddleware.middleware)
});
setupListeners(store.dispatch);
