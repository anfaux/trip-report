import React from "react";
import { useGetStatisticsQuery } from "../services/tripReportApi";
import humanizeDuration from "humanize-duration";

const Statistics = () => {
  const { data, isLoading } = useGetStatisticsQuery();
  if (!data || isLoading)
    return <div style={{ height: "5rem" }}>Loading Statistics...</div>;
  return (
    <React.Fragment>
      Covering: {humanizeDuration(data.start_date - data.end_date)}
      <table>
        <tbody>
          <tr>
            <td>Worlds Visited</td>
            <td>{data.total_worlds_visited.toLocaleString("en-US")}</td>
          </tr>
          <tr>
            <td>Players Encountered</td>
            <td>{data.total_player_encounters.toLocaleString("en-US")}</td>
          </tr>
          <tr>
            <td>Media Played</td>
            <td>{data.total_media.toLocaleString("en-US")}</td>
          </tr>
          <tr>
            <td>Records</td>
            <td>{data.total_records.toLocaleString("en-US")}</td>
          </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
};
export default Statistics;
