import React, { useState, useRef, useEffect } from "react";
import Mousetrap from "mousetrap";
import { DateTime } from "luxon";
import JSZip from "jszip";
import JSZipUtils from "jszip-utils";

const parseVrchatScreenshotName = (fileName) => {
  if (!fileName) return null;
  const fileNameSplit = fileName.replaceAll("NYE23-", "NYE23_").split("_");
  const metaData = fileNameSplit[1].includes("x")
    ? {
        month: fileNameSplit[2]?.split("-")[1],
        day: fileNameSplit[2]?.split("-")[2],
        year: fileNameSplit[2]?.split("-")[0],
        width: fileNameSplit[1]?.split("x")[0],
        height: fileNameSplit[1]?.split("x")[1]
      }
    : {
        month: fileNameSplit[1]?.split("-")[1],
        day: fileNameSplit[1]?.split("-")[2],
        year: fileNameSplit[1]?.split("-")[0],
        width: parseInt(fileNameSplit[3]?.split("x")[0], 10),
        height: parseInt(fileNameSplit[3]?.split("x")[1], 10)
      };
  return metaData;
};

const zipAndDownload = async (filesToZip) => {
  console.log(`Zipping ${filesToZip.length} files...`);
  var zip = new JSZip();
  var count = 0;
  filesToZip.forEach((file, idx) => {
    JSZipUtils.getBinaryContent(file.url, (err, data) => {
      if (err) throw err;
      zip.file(file.name, data, { binary: true });
      count++;
      if (count === filesToZip.length) {
        zip.generateAsync({ type: "blob" }).then(async (content) => {
          const url = URL.createObjectURL(content);
          const link = document.createElement("a");
          link.href = url;
          link.download = `archive.zip`;
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        });
      }
    });
  });
};
const downloadFile = (url, fileName) => {
  fetch(url, { method: "get" })
    .then((res) => res.blob())
    .then((res) => {
      console.log(res);
      const aElement = document.createElement("a");
      aElement.setAttribute("download", fileName);
      const href = URL.createObjectURL(res);
      aElement.href = href;
      aElement.setAttribute("target", "_blank");
      aElement.click();
      URL.revokeObjectURL(href);
    });
};

const useOutsideAlerter = (ref, onOutsideClick) => {
  useEffect(() => {
    function handleClickOutside(e) {
      if (ref.current && !ref.current.contains(e.target)) onOutsideClick(e);
    }
    document.addEventListener("mousedown", handleClickOutside);
    return () => document.removeEventListener("mousedown", handleClickOutside);
  }, [onOutsideClick, ref]);
};

const Zoomed = ({ counter, currentImage, onOutsideClick, onNext, onPrev }) => {
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef, (e) => {
    if (e.target.className !== "button") onOutsideClick();
  });
  Mousetrap.bind("esc", onOutsideClick, "keyup");
  Mousetrap.bind("right", onNext, "keyup");
  Mousetrap.bind("left", onPrev, "keyup");

  if (!currentImage) return null;
  const metaData = parseVrchatScreenshotName(currentImage.data.fileName);
  const imgSrcPath = `${process.env.REACT_APP_TRIPREPORT_SERVICE}/assets/${
    metaData.year
  }/${metaData.month}/${currentImage.data.fileName.replace(".png", "")}`;
  const imgSrc_original = `${imgSrcPath}/original.png`;
  const imgSrc_preview = `${imgSrcPath}/preview.png`;
  return (
    <div style={styles.zoomContainer}>
      <div style={styles.zoomNavbar}>
        <div>
          <div>{counter}</div>
          <div>
            {DateTime.fromMillis(currentImage.ts).toLocaleString(
              DateTime.DATETIME_FULL
            )}
          </div>
        </div>
        <div style={{ flexGrow: 1 }} />
        <div
          className="button"
          onClick={(e) =>
            downloadFile(imgSrc_original, currentImage.data.fileName)
          }
        >
          [download]
        </div>
      </div>
      <div style={styles.zoomModalOverlay}>
        <div ref={wrapperRef} style={styles.zoomImgWrapper}>
          <img
            onClick={(e) => {
              if (e.clientX < window.innerWidth / 2) {
                onPrev();
              } else {
                onNext();
              }
            }}
            alt={currentImage.data.fileName}
            style={styles.zoomImg}
            src={imgSrc_preview}
          />
        </div>
      </div>
    </div>
  );
};

const Gallery = ({ screenshots }) => {
  const [prevImage, setPrevImage] = useState(null);
  const [nextImage, setNextImage] = useState(null);
  const [currentImage, _setCurrentImage] = useState(null);
  const setCurrentImage = (value) => {
    const nextIdx =
      screenshots.indexOf(value) + 1 < screenshots.length
        ? screenshots.indexOf(value) + 1
        : 0;
    const prevIdx =
      screenshots.indexOf(value) - 1 >= 0
        ? screenshots.indexOf(value) - 1
        : screenshots.length - 1;
    console.log(`${prevIdx} - ${screenshots.indexOf(value)} - ${nextIdx}`);

    setNextImage(screenshots[nextIdx]);
    setPrevImage(screenshots[prevIdx]);
    _setCurrentImage(value);
  };
  return (
    <div>
      <Zoomed
        counter={`${screenshots.indexOf(currentImage) + 1} of ${
          screenshots.length
        }`}
        currentImage={currentImage}
        onNext={() => setCurrentImage(nextImage)}
        onPrev={() => setCurrentImage(prevImage)}
        onOutsideClick={() => setCurrentImage(null)}
      />
      <div>
        <div>
          <h3>{screenshots.length} screenshots</h3>
        </div>
        <div
          style={{ marginRight: "3rem" }}
          onClick={(e) => {
            zipAndDownload(
              screenshots.map((screenshot) => {
                const metaData = parseVrchatScreenshotName(
                  screenshot.data.fileName
                );
                const url = `${
                  process.env.REACT_APP_TRIPREPORT_SERVICE
                }/assets/${metaData.year}/${
                  metaData.month
                }/${currentImage.data.fileName.replace(
                  ".png",
                  ""
                )}/thumbnail.png`;
                return { name: screenshot.data.fileName, url };
              })
            );
          }}
        >
          [download]
        </div>
      </div>
      <div style={styles.gallery}>
        {screenshots.map((image, idx) => {
          const metaData = parseVrchatScreenshotName(image.data.fileName);
          const thumbnailUrl = `${
            process.env.REACT_APP_TRIPREPORT_SERVICE
          }/assets/${metaData.year}/${
            metaData.month
          }/${image.data.fileName.replace(".png", "")}/thumbnail.png`;
          return (
            <div
              onClick={() => setCurrentImage(image)}
              key={idx}
              style={styles.galleryImgWrapper}
            >
              <img
                draggable={false}
                style={styles.galleryImg}
                alt={image.data.fileName}
                src={thumbnailUrl}
                onError={({ currentTarget }) => {
                  console.log(currentTarget.src);
                  currentTarget.onerror = null;
                  currentTarget.src = "/missing.png";
                }}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default Gallery;
const styles = {
  gallery: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    flexWrap: "wrap"
  },
  galleryImgWrapper: {
    width: "150px",
    height: "100px",
    overflow: "hidden",
    margin: ".2rem"
  },
  galleryImg: { width: "100%", height: "100%", objectFit: "cover" },
  zoomContainer: {
    position: "fixed",
    top: 0,
    left: 0,
    zIndex: 2,
    width: "100vw",
    height: "100vh",
    background: "rgba(0,0,0,.8)",
    display: "flex",
    alignItems: "center"
  },
  zoomNavbar: {
    display: "flex",
    alignItems: "center",
    padding: "2rem",
    flexDirection: "row",
    height: "3rem",
    width: "calc(100vw - 4rem)",
    position: "absolute",
    top: 0,
    fontWeight: 900,
    color: "white",
    zIndex: 5,
    background: "rgba(0, 0, 0, 0.5)"
  },
  zoomModalOverlay: {
    display: "flex",
    margin: "auto",
    padding: "3rem",
    alignItems: "center"
  },
  zoomImgWrapper: {
    boxShadow: " rgba(0, 0, 0, 0.16) 0px 1px 4px",
    maxHeight: "90vh"
  },
  zoomImg: {
    width: "100%",
    maxHeight: "90vh",
    boxShadow: " rgba(0, 0, 0, 0.16) 0px 1px 4px"
  }
};
