# Trip Report Beta v2.0

Collects VRC Logs and converts them to a database, also provides a web front-end to browse the logs as a series of "trip reports."

> **THIS IS VERY DIFFERENT THAN v1.0**  
> V1.0 of this project used scripts to generate markdown that 11ty turned into a static website. V2.0 runs a local server which ingests the logs into a SQL database, monitors logfiles and serves up a React-based website. V1 is available at the v1.0 tag.

☕ &nbsp; _Is this project helpful to you? [Please consider buying me a coffee](https://pay.feralresearch.org/tip)._

# How does this work?

- TripReport runs on your computer and presents a dynamic website (defaults to [http://localhost:8888](http://localhost:8888)) and an api.
- When you exit VRChat, the latest logs and images are ingested by TripReport into a SQL Database and the website updates itself.
- There are many configuration options (see below), all are managed using the `.env` file
- Additional tools are provided in `src/utilities` for bulk import and parsing/importing logs (you don't need to run the server if you just want to get logs into a database)

# Related Projects?

[WarpTrail](https://github.com/ticky/warptrail)  
Is very similar, written in Python, and does a cool thing with the system tray!

[VRCX](https://github.com/vrcx-team/VRCX)  
Well known companion app. Doesn't quite do what this does, but worth noting it also parses logs into a database. At `%appData%/VRCX/VRCX.sqlite3`

# Configuration

The app is configured using .env file

Recommended default: "Watch log files and images, keep backups, only import what you need, skip importing stuff you already have"

```shell
DEBUG=true
PORT=8888
VRCHAT_PROCESS_NAME="VRChat.exe"

DIR_VRC_LOG_FILES="%USERPROFILE%\AppData\LocalLow\VRChat\VRChat"
DIR_VRC_SCREENSHOTS="%USERPROFILE%\Pictures\VRChat"
DIR_DATA="./DATA"

WATCHER_ENABLED=true
WATCHER_REMOVE_AFTER_IMPORT=false
WATCHER_BACKUP_AFTER_IMPORT=true

DB_FORCE_REBUILD=false
DB_ANNOTATE=true
DB_OPTIMIZE=true

SCREENSHOTS_MANAGE=true
SCREENSHOTS_FORCE_REBUILD=false
```

**`DEBUG` (Optional - Default: false)**  
Enables verbose logging

**`PORT` (Optional - Default: 8888)**  
Trip Report will serve a managment website at `http://localhost:PORT` and its own API at `http://localhost:PORT/api`

**`DIR_VRC_LOG_FILES` (Required)**  
Directory where VRChat normally writes log files. Typically `%AppData%\..\LocalLow\VRChat\vrchat\`.

**`DIR_VRC_SCREENSHOTS (Required if SCREENSHOTS_MANAGE=true)`**  
Directory where VRChat normally stores screenshots. TripReport WILL recurse subdirs. Typically `%USERPROFILE%\Pictures\VRChat`.

**`DIR_DATA` (Required)**  
Directory where TripReport stores data. This includes the database if using SQLite, images and log backups.

**`WATCHER_ENABLED` (Optional, default: false)**  
If set to "false" then the watcher capability of Trip Report will not be enabled. The website will still be served, but neither ingesting nor backups will happen automatically.

**`WATCHER_REMOVE_AFTER_IMPORT` (Optional, default: false)**  
 If "true," Trip Report will delete the logs out of the source directory after it has finished importing them. It is recommended that if you set this true you also set `WATCHER_BACKUP_AFTER_IMPORT` to true. Please note that VRChat itself will delete the logfiles every 24hours anyway! Per the official documentation:

> Your logs are kept for 24 hours, where the HH-MM-SS portion of the filename describes the time at which you launched the application for that log. When VRChat launches, it checks to see if any of the logs are older than 24 hours. If there are any, it deletes them."

**`WATCHER_BACKUP_AFTER_IMPORT` (Optional, default: false)**  
Default is "false." If "true," Trip Report will create a folder called "backup" inside your data directory and will keep a copy of every log file that it has imported. VRChat normally deletes logs after 24 hours, so you probably want this set to true!

**`DB_ANNOTATE` (Optional, default: false)**  
Trip report annotates log entires in order to generate the website more efficiently. Under normal operation you want this to be true. If you are using only the bulk tools to convert logs to SQL, you might consider turning this off (false) so that the database contains only the original log entries with no additional information.

**`DB_OPTIMIZE` (Optional, default: false)**  
By default, TripReport will import ALL entries from the original log files. This will preserve your entire log history, and accessing this data with queires if intersting, but log files can be huge and most of these entries aren't useful for creating the website. If `DB_OPTIMIZE` is set to true, the tool will only import records which it needs. Note that if `DB_ANNOTATE` is set to false and `DB_OPTIMIZE` is set to true, you will end up with an empty database. Also if you set `DB_OPTIMIZE` to true you should probably also set WATCHER_BACKUP_AFTER_IMPORT to true so that you keep a full copy of every log, in case you want to access that data in the future.

**`SCREENSHOTS_MANAGE` (Optional, default: false)**  
If "false" then TripReport will skip ingesting images. Image processing is the slowest part of this tool. Skipping images will result in much faster imports but galleries on the website will not work.

**`DB_FORCE_REBUILD`**  
Only used by the bulk-import utility. If true, this will recreate the database from scratch on every run. Existing database will be backed up.

**`SCREENSHOTS_FORCE_REBUILD`**  
Only used by the bulk-import utility.If true, this will re-import all images on every run. All screenshots will be copied again, all thumbnails will be regenerated.

# Where are my logs normally?

[[From the VRC Docs](https://docs.vrchat.com/docs/frequently-asked-questions)]

> "The VRChat output logs are located at C:\Users\[Username]\AppData\LocalLow\VRChat\vrchat. The output log is named output_log-HH-MM-SS.txt. You can quickly find this directory by pasting this into your Windows Explorer address bar:

> `%AppData%\..\LocalLow\VRChat\vrchat\`

> Your logs are kept for 24 hours, where the HH-MM-SS portion of the filename describes the time at which you launched the application for that log. When VRChat launches, it checks to see if any of the logs are older than 24 hours. If there are any, it deletes them."

##

# What if I want to use Postgres instead of SQLite?

Database access is managed via Knex, so many things are possible! To switch to Postgres, provide a `PG_CONNECTION_STRING` variable in .env. Note that this will create a fresh database on first run, it will not import any records that you already have in your SQLite DB.

# What if I have existing logs or images I want to add?

I have provided two bulk import tools, one for logs and one for images. While it would technically be possible to stuff your existing logs and images into the default folders and let the main server parse them on the first run, you probably shouldn't do this. The bulk tools are written differently and are far more efficient.

These tools do their best to avoid duplicate records, but this is a tricky problem - It is advisable to run TripReport at least once to create an empty DB, then run these two tools _once_ on existing logs and images. Subsequent updates should happen automatically from there.

### 1. Bulk import Logs

Input: `%DIR_VRC_LOG_FILES%`  
Output: `%DIR_DATA%`

```sh
node ./src/utilities/bulkImportLogs.js --max-old-space-size=8000
```

### 2. Bulk import Screenshots

Input: `%DIR_VRC_LOG_FILES%`  
Output: `%DIR_DATA%/assets`

```sh
node ./src/utilities/bulkImportScreenshots.js --max-old-space-size=8000
```

### TXT -> JSON

Normally not used, but useful if you just want to convert a pile of VRChat logs in their native .txt format into structured JSON. Doesn't touch the database.  
Input: `%DIR_VRC_LOG_FILES%`  
Output: `%DIR_DATA%/json`

```sh
node ./src/utilities/txt2json.js --max-old-space-size=8000
```

### JSON -> DB

Normally not used, but useful if you want to import a pile of VRChat logs in JSON format (see above command) into the database.  
Input: `%DIR_VRC_LOG_FILES%/json`  
Output: `%DIR_DATA%/database.db`

```sh
node ./src/utilities/json2db.js --max-old-space-size=8000
```

## Can I update the website itself?

Yes for sure. For convenience there is a built copy of the database located in `src/server/public`. You may replace this with whatever you like.

The sourcecode for that website is located in `src/client` which is a react app initialized by [create-react-app](https://create-react-app.dev/). For convenience you can develop inside of the client directory (`yarn run` to run a dev copy at port 3000). Whenever you are ready to update the trip report copy you can run `yarn build` which will publish updates to `src/server/public.`

# Known Issues / ToDo

## WQL Query Fail

There is an issue with the WQL watcher library that crops up if you repeatedly register a listener. Under normal operation you shouldn't see it, but if you spam the subscription (by doing development and running `nodemon`, for instance) you might run into this. Currently the only solution is to reboot the machine and/or wait a few mintues and try again. You might consider `WATCHER_ENABLED=false` while doing dev work (unless that's what you're working on of course)

```sh
node:internal/process/promises:288
            triggerUncaughtException(err, true /* fromPromise */);
            ^

Failure [ERR_WQL_QUERY_FAIL]: Unexpected COM interface failure, please check your usage and settings.
    at async file:///.../trip-report/src/server/app.js:47:32 {
  code: 'ERR_WQL_QUERY_FAIL'
}

```
